﻿using System.Collections.Generic;
namespace TypingTest.Models.Home {
	public class Topic {
		public int Id { get; set; }
		public string Title { get; set; }
		public string Text { get; set; }
	}
	public class DefaultModel {
		public List<Topic> Topics { get; set; }

		public DefaultModel() {
			Topics = new List<Topic> {
				new Topic {
					Id = 1,
					Title = "Text 1",
					Text = "avhj klearvuio hgjerhg fdkhgfjvg gvh dkjgj"
				},
				new Topic {
					Id = 2,
					Title = "Text 2",
					Text = "aghd ghj h vhj kjhkg hjkg arvuio hgj gjkerhg f jkdkhgfj"
				}
			};
		}
	}
}