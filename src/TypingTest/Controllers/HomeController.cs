﻿using Microsoft.AspNetCore.Mvc;
using TypingTest.Models.Home;
namespace TypingTest.Controllers {
	public class HomeController : Controller {
		public IActionResult Index() {
			return View(new DefaultModel());
		}
	}
}