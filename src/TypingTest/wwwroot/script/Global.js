var topics = [
    {
        id: 1,
        title: 'Turing test',
        text: 'The Turing test is a test, developed by Alan Turing in 1950, of a machine\'s ability to exhibit intelligent behaviour equivalent to, or indistinguishable from, that of a human. Turing proposed that a human evaluator would judge natural language conversations between a human and a machine that is designed to generate human-like responses. The evaluator would be aware that one of the two partners in conversation is a machine, and all participants would be separated from one another. The conversation would be limited to a text-only channel such as a computer keyboard and screen so that the result would not be dependent on the machine\'s ability to render words as speech. If the evaluator cannot reliably tell the machine from the human (Turing originally suggested that the machine would convince a human 70% of the time after five minutes of conversation), the machine is said to have passed the test. The test does not check the ability to give correct answers to questions, only how closely answers resemble those a human would give. The test was introduced by Turing in his paper, "Computing Machinery and Intelligence", while working at the University of Manchester. It opens with the words: "I propose to consider the question, \'Can machines think?\'" Because "thinking" is difficult to define, Turing chooses to "replace the question by another, which is closely related to it and is expressed in relatively unambiguous words." Turing\'s new question is: "Are there imaginable digital computers which would do well in the imitation game?" This question, Turing believed, is one that can actually be answered. In the remainder of the paper, he argued against all the major objections to the proposition that "machines can think".'
    }, {
        id: 2,
        title: 'Harry Potter',
        text: 'Harry Potter is a series of fantasy novels written by British author J. K. Rowling. The novels chronicle the life of a young wizard, Harry Potter, and his friends Hermione Granger and Ron Weasley, all of whom are students at Hogwarts School of Witchcraft and Wizardry. The main story arc concerns Harry\'s struggle against Lord Voldemort, a dark wizard who intends to become immortal, overthrow the wizard governing body known as the Ministry of Magic, and subjugate all wizards and Muggles. Since the release of the first novel, Harry Potter and the Philosopher\'s Stone, on 26 June 1997, the books have found immense popularity, critical acclaim and commercial success worldwide. The series has now been translated into multiple languages including Irish, Spanish, French, German and Swedish to name a few. They have attracted a wide adult audience as well as younger readers, and are often considered cornerstones of modern young adult literature. The series has also had its share of criticism, including concern about the increasingly dark tone as the series progressed, as well as the often gruesome and graphic violence it depicts. As of May 2013, the books have sold more than 500 million copies worldwide, making them the best-selling book series in history, and have been translated into seventy-three languages. The last four books consecutively set records as the fastest-selling books in history, with the final instalment selling roughly eleven million copies in the United States within twenty-four hours of its release.'
    }, {
        id: 3,
        title: 'Pink Floyd',
        text: 'Pink Floyd were an English rock band formed in London. They achieved international acclaim with their progressive and psychedelic music. Distinguished by their use of philosophical lyrics, sonic experimentation, extended compositions and elaborate live shows, they are one of the most commercially successful and influential groups in the history of popular music. Pink Floyd were founded in 1965 by students Syd Barrett on guitar and lead vocals, Nick Mason on drums, Roger Waters on bass and vocals, and Richard Wright on keyboards and vocals. They gained popularity performing in London\'s underground music scene during the late 1960s, and under Barrett\'s leadership released two charting singles and a successful debut album, The Piper at the Gates of Dawn (1967). Guitarist David Gilmour joined in December 1967; Barrett left in April 1968 due to deteriorating mental health. Waters became the band\'s primary lyricist and eventually their dominant songwriter, devising the concepts behind their albums The Dark Side of the Moon (1973), Wish You Were Here (1975), Animals (1977), The Wall (1979) and The Final Cut (1983). The Dark Side of the Moon and The Wall became two of the best-selling albums of all time.'
    }
];
$(document).ready(function () {
    var isTesting;
    var testTotalTime;
    var timeLeft;
    var timerInterval;
    $('#Test').hide();
	topics.forEach(function(topic) {
		$('#Topic').append('<option value="' + topic.id + '">' + topic.title + '</option>');
	});
	$('#TopicText').text(topics[0].text);
    function prettifyTimeDuration(timeLeft) {
        var minutes = timeLeft.minutes();
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var seconds = timeLeft.seconds();
        seconds = seconds < 10 ? '0' + seconds : seconds;
        return minutes + ':' + seconds;
    }
    $('#Topic').on('change', function () {
        var topicId = parseInt($('#Topic option:selected').attr('value'), 10);
        var topic = topics.filter(function (t) { return t.id === topicId; })[0];
        $('#TopicText').text(topic.text);
    });
    $('#StartTestBtn').on('click', function () {
    	isTesting = false;
    	var topicId = parseInt($('#Topic option:selected').attr('value'), 10);
    	var topic = topics.filter(function (t) { return t.id === topicId; })[0];
    	$('#SourceText').text(topic.text);
    	testTotalTime = parseInt($('#TimeLimit option:selected').attr('value'), 10);
    	timeLeft = moment.duration(testTotalTime, 'minutes');
    	$('#TimeLeft').text(prettifyTimeDuration(timeLeft));
        $('#Settings').hide();
        $('#Test').show();
    });
    function getTestResult() {
        var wrongLetters = 0;
        var wrongWords = 0;
        var sourceWords = $('#SourceText').val().split(' ');
        var typedWords = $('#TypingArea').val().split(' ');
        for (var i = 0; i < (typedWords.length < sourceWords.length ? typedWords.length : sourceWords.length); i++) {
            var sourceWord = sourceWords[i];
            var typedWord = typedWords[i];
            var wrongLettersInWord = 0;
            for (var j = 0; j < (typedWord.length > sourceWord.length ? typedWord.length : sourceWord.length); j++) {
                if (sourceWord[j] !== typedWord[j]) {
                    wrongLettersInWord++;
                }
            }
            if (wrongLettersInWord !== 0) {
                wrongLetters += wrongLettersInWord;
                wrongWords++;
            }
        }
        var spentMinutes = testTotalTime - timeLeft.minutes() - timeLeft.seconds() / 60;
        var lettersTyped = $('#TypingArea').val().length;
        var correctLettersTyped = lettersTyped - wrongLetters;
        var wordsTyped = typedWords.length < sourceWords.length ? typedWords.length : sourceWords.length;
        var correctWordsTyped = wordsTyped - wrongWords;
        return {
            spentTime: moment.duration(testTotalTime * 60 - timeLeft.minutes() * 60 - timeLeft.seconds(), 'seconds'),
            wrongLetters: Math.floor(wrongLetters / $('#TypingArea').val().length * 100),
            wrongWords: Math.floor(wrongWords / typedWords.length * 100),
            lpm: Math.floor(correctLettersTyped / spentMinutes),
            wpm: Math.floor(correctWordsTyped / spentMinutes)
        };
    }
    function stop() {
        clearInterval(timerInterval);
        var testResult = getTestResult();
        $('#SpentTime').text(prettifyTimeDuration(testResult.spentTime));
        $('#Speed').text(testResult.wpm + ' words per minute (' + testResult.lpm + ' letters per minute)');
        $('#Mistakes').text(testResult.wrongWords + '% wrong words (' + testResult.wrongLetters + '% letters)');
        $('#Test').hide();
        $('#Results').show();
    }
    $('#TypingArea').on('keypress', function () {
        if (!isTesting) {
        	isTesting = true;
	        var sourceText = $('#SourceText');
	        var typedText = $('#TypingArea');
            timerInterval = setInterval(function () {
                timeLeft.add(-1, 'seconds');
                if (timeLeft.minutes() <= 0 && timeLeft.seconds() <= 0 || sourceText.val() === typedText.val()) {
                    stop();
                }
                $('#TimeLeft').text(prettifyTimeDuration(timeLeft));
            }, 1000);
        }
    });
    $('#StopBtn').on('click', function () {
        stop();
    });
    $('#TryAgain').on('click', function () {
        location.reload();
    });
});